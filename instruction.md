#!/bin/bash
if [[ $EUID -ne 0 ]]; then
   	echo "This script must be run as root"
   	exit 1
else
        #Update
	echo "Updating"
	apt-get update
        
        #Installing php and php-fpm
        echo "Installing php and php-fpm"
        apt-get install nginx php php-fpm -y
        
        #Installing composer
        echo "Installing composer"
        php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
        php -r "if (hash_file('sha384', 'composer-setup.php') === '55ce33d7678c5a611085589f1f3ddf8b3c52d662cd01d4ba75c0ee0459970c2200a51f492d557530c71c15d8dba01eae') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
        php composer-setup.php
        php -r "unlink('composer-setup.php');"
        sudo mv composer.phar /usr/local/bin/composer

        #Installing CLI symfony
        echo "Installing CLI"
        curl -1sLf 'https://dl.cloudsmith.io/public/symfony/stable/setup.deb.sh' | sudo -E bash
        sudo apt install symfony-cli -y
        
        #Installing requirements 
        echo "Installing requirements"
        apt-get install php8.1-xml php8.1-mysql php8.1-sqlite php-mbstring php8.1-intl zip unzip php-zip -y 
        
        #Create project
        echo "Create project"
        composer create-project symfony/symfony-demo my_project
        
        #Installing mysql database
        echo "Installing mysql database"
        sudo apt-get install mysql-server -y
        sudo apt install mysql-client -y
        mysql -uroot <<MYSQL_SCRIPT
        CREATE DATABASE do2 CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
        CREATE USER 'api'@'localhost' IDENTIFIED BY '12345';
        GRANT ALL PRIVILEGES ON do2.* TO 'api'@'localhost';
        FLUSH PRIVILEGES;
MYSQL_SCRIPT
        echo "MySQL database created."
        echo "Database:   do2"
        echo "Username:   api"
        echo "Password:   12345"
        fi
